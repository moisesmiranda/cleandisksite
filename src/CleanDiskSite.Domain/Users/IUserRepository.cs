﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanDiskSite.Domain.Users
{
    public interface IUserRepository
    {
        void Save(User user);
        void Delete(User user);
        void Update(User user);
        User Get(string email);
    }
}
