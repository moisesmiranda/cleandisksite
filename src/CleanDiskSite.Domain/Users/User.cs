﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanDiskSite.Domain.Users
{
    public class User
    {
        public User(string name, string email)
        {

        }

        protected User()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public int DepartmentId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// Senha nunca expira
        /// </summary>
        public bool PasswordNeverExp { get; set; }
        public bool Locked { get; set; }

        public void SetPassword(string password, string confirmPassword)
        {

        }

        public string ResetPassword()
        {
            return string.Empty;
        }
    }
}
